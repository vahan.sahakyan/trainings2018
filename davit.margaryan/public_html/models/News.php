<?php

namespace Models;
use Libs\Model;

///////////////////////////////////////////////////////////////////
// 
// class News extends Model
//
// Work with News table in database
//
// public function __construct()
// public function get($id = NULL)
// 
// private $News = [];
//
///////////////////////////////////////////////////////////////////

class News extends Model
{
    /**
     * The news from db
     */
    private $news_ = [];

    /**
     * Constructor calls parent constructor
     * and gets connectvity for db
     */
    function __construct()
    {
        // TODO: Save it in db
        $this->news_ = [
            1 => [
                'header'  => 'What is OpemGL?',
                'image' => "http://techtoggle.com/wp-content/uploads/2009/01/opengl-logo.gif",
                'content' => 
                "Lorem Ipsum is simply dummy text of the printing 
                and typesetting industry. Lorem Ipsum has been the 
                industry's standard dummy text ever since the 1500s, 
                when an unknown printer took a galley of type and scrambled 
                it to make a type specimen book. It has survived not only five
                centuries, but also the leap into electronic typesetting, 
                remaining essentially unchanged. It was popularised in the 
                1960s with the release of Letraset sheets containing Lorem 
                Ipsum passages, and more recently with desktop publishing 
                software like Aldus PageMaker including versions of Lorem Ipsum."
            ],
            2 => [
                'header'  => 'What is OpemGL?',
                'image'   => "https://qt-web-uploads.s3.amazonaws.com/wp-content/uploads/2016/05/TheQtCompany_logo_1200x630.png",
                'content' => 
                "Lorem Ipsum is simply dummy text of the printing 
                and typesetting industry. Lorem Ipsum has been the 
                industry's standard dummy text ever since the 1500s, 
                when an unknown printer took a galley of type and scrambled 
                it to make a type specimen book. It has survived not only five
                centuries, but also the leap into electronic typesetting, 
                remaining essentially unchanged. It was popularised in the 
                1960s with the release of Letraset sheets containing Lorem 
                Ipsum passages, and more recently with desktop publishing 
                software like Aldus PageMaker including versions of Lorem Ipsum."
            ]
        ];
        parent::__construct();
    }

    /**
     * Returns the news from db
     *
     * @param int $id - the current id of news
     * @return array $this->news_ - the news array from db
     *         | current news which id = $id | NULL
     */
    public function get($id = NULL)
    {
        if (is_null($id)) {
            return $this->news_;
        }
        return NULL;
    }
}
?>
