<?php
    echo '<section>';
    if (!is_null($this->get('news'))) {
        echo '<div class="article">';
        foreach ($this->get('news') as $news) {
            echo '<div class="post-header"><h1><a href="#"> '.$news['header'].'</a></h1></div>';
            echo '<div class="post-section">';
            echo     '<div class="post-image">';
            echo         '<img title="OpenGL" alt="OpenGL" src="'.$news['image'].'" />';
            echo     '</div>';
            echo     '<div class="post-text">';
                      echo $news['content'];
            echo     '</div>';
            echo '</div>';
            echo '<div class="post-footer">';
            echo     '<a href="#">Read More..</a>';
            echo '</div>';
        }
        echo '</div>';
        /* This must be dynamic with news */
        echo '<div class="pagination-bar">';
        echo     '<ul>';
        echo         '<li><a href="#"><-</a></li>';
        echo         '<li>1</li>';
        echo         '<li>2</li>';
        echo         '<li>3....</li>';
        echo         '<li><a href="#">-></a></li>';
        echo     '</ul>';
        echo '</div>';
    }
    echo '</section>';
?>
