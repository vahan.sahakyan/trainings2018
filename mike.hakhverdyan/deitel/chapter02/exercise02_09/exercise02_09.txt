Exercise 2.9

Write a single C++ statement or line that accomplishes each of the following:
a. Print the message "Enter two numbers".
b. Assign the product of variables b and c to variable a.
c. State that a program performs a payroll calculation (i.e., use text that helps to document a program).
d. Input three integer values from the keyboard into integer variables a, b and c.

Answer:

a. std::cout << "Enter two numbers: " << std::endl;
b. c = a + b;
c. ///program is doing the calculations
d. std::cin >> a >> b >> c;
