/// Exercise4_18
/// Program for calculation.
#include <iostream>
#include <unistd.h>

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Please enter an integer for 'N' " << std::endl;
    }
    int number = 1;
    int nValue = 0;
    std::cin >> nValue;
    std::cout << "N\t" << "N*10\t" << "N*100\t" << "N*1000 " << std::endl;
    while (number <= nValue) {
        std::cout << number << "\t" << number * 10 << "\t" << number * 100 << "\t" << number * 1000 << std::endl;
        ++number;
    }
    return 0; /// indicate successful termination
} /// end main

