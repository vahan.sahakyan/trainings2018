/// Exercise4_14
/// Program that will determine whether a department-store customer has exceeded the credit limit on a charge account.
#include <iostream>
#include <iomanip>   /// input output manipulation header for using setprecision () manipulator
#include <unistd.h>

int
main()
{
    while (true) {
        if (::isatty(STDIN_FILENO)) {
            /// prompting user to input account number or -1 to exit the program
            std::cout << "Enter account number or -1 to quit! " << std::endl;
        }
        int accountNumber = 0;
        std::cin >> accountNumber;
        if (accountNumber <= 0) {
            /// if inputed invalid account number the program stops
            std::cout << "Error 1: Invalid account number. Input positive number! " << std::endl;
            return 1;
        } 
        if (-1 == accountNumber) {
            /// end program when inputed -1 as sentinel number
            std::cout << "Thank you for using our program " << std::endl;
            return 0;
        }

        if (::isatty(STDIN_FILENO)) {
            /// prompts user to input beginning ballance, it's able to input with decimal point
            std::cout << "Enter beginning ballance " << std::endl;
        }
        double beginningBalance = 0;
        std::cin >> beginningBalance;  /// in real the beginning ballance can be positive
        if (::isatty(STDIN_FILENO)) {
            /// prompts user to input total charges, it's able to input with decimal point
            std::cout << "Enter total charges " << std::endl;
        }
        double totalCharges = 0;
        std::cin >> std::fixed >> totalCharges;
        if (totalCharges < 0) {
            /// if inputed invalid total charge value the program stops
            std::cout << "Error 2: Invalid total charges value. Should be 0 or above! " << std::endl;
            return 2;
        }
        if (::isatty(STDIN_FILENO)) {
            /// prompts user to input total credits, it's able to input with decimal point
            std::cout << "Enter total credits " << std::endl;
        }
        double totalCredits = 0;
        std::cin >> std::fixed >> totalCredits;
        if (totalCredits < 0) {
            /// if inputed invalid total credits value the program stops
            std::cout << "Error 3: Invalid total credits value. Should be 0 or above! " << std::endl;
            return 3;
        }
        if (::isatty(STDIN_FILENO)) {
            /// prompts user to input credit limit, it's able to input with decimal point
            std::cout << "Enter credit limit " << std::endl;
        }
        double creditLimit = 0;
        std::cin >> std::fixed >> creditLimit;
        if (creditLimit < 0) {
            /// if inputed invalid credit limit value the program stops
            std::cout << "Error 4: Invalid credit limit value. Should be 0 or above! " << std::endl;
            return 4;
        }
        double newBalance = beginningBalance + totalCharges - totalCredits;
        std::cout << "New balance is " << std::setprecision (2) << std::fixed << newBalance << std::endl;
        /// prints new balance set with 2 digit numeric precision after decimal point

        if (newBalance > creditLimit) {
            /// if credit limit is exceeded the program prints this info
            std::cout << "Account: " << std::setprecision (2) << std::fixed << accountNumber << std::endl;
            std::cout << "Credit Limit: " << std::setprecision (2) << std::fixed << creditLimit << std::endl;
            std::cout << "Balance: " <<  std::setprecision (2) << std::fixed << newBalance << std::endl;
            std::cout << "Credit Limit Exceeded. " << std::endl;
        }
    }  /// the end of while loop
    return 0; /// indicate successful termination
} /// end main

