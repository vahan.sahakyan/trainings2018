std::cin is standard input stream object, alongside with stream extraction operator >> the object reads input from keyboard using, and that input must be stored in a variable in the memory of computer to be used.

std::cout is standard output stream object, alongside with stream insertion operator << the object prints the data (texts, streams of characters, variables and so on) to the screen.
