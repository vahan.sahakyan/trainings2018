#ifndef __SOME_CLASS_IMPL__
#define __SOME_CLASS_IMPL__

class SomeClassImpl
{
public:
    SomeClassImpl(const int someVariable);
    void someFunction();
private:
    void someAnotherFunction();
private:
    int someVariable_;
};

#endif /// __SOME_CLASS_IMPL__

