#include "headers/core/ChatConnection.hpp"
#include "headers/core/ChatServer.hpp"

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <cstring>
#include <iostream>

const int PORT = 8080;
const int MESSAGE_SIZE = 1024;

ChatConnection::ChatConnection()
{
}

ChatConnection::ChatConnection(ChatServer* server, const int clientSocket)
    : server_(server)
    , mainSocket_(clientSocket)
{
    thread_ = std::thread(&ChatConnection::execute, this);
}

void
ChatConnection::execute()
{
    while (true) {
        int readSize;
        const int valread1 = ::recv(mainSocket_, &readSize, sizeof(readSize), MSG_WAITALL);
        /// std::cout << "valread = " << valread1 << ", readSize = " << readSize << std::endl;
        if (valread1 != sizeof(readSize)) {
            exit(0);
        }

        char message[MESSAGE_SIZE];
        const int valread2 = ::recv(mainSocket_, message, readSize, MSG_WAITALL);
        /// std::cout << "valread = " << valread2 << ", message = " << message << std::endl;
        if (valread2 != readSize) {
            exit(0);
        }

        server_->sendToChatRoom(message, readSize);
    }
}

void
ChatConnection::receive()
{
    while (true) {
        int readSize;
        const int valread1 = ::recv(mainSocket_, &readSize, sizeof(readSize), MSG_WAITALL);
        /// std::cout << "valread = " << valread1 << ", readSize = " << readSize << std::endl;
        if (valread1 != sizeof(readSize)) {
            continue;
        }

        char message[MESSAGE_SIZE];
        const int valread2 = ::recv(mainSocket_, message, readSize, MSG_WAITALL);
        /// std::cout << "valread = " << valread2 << ", message = " << message << std::endl;
        if (valread2 != readSize) {
            continue;
        }

        std::cout << " -> " << message << std::endl;
    }
}

int
ChatConnection::connect()
{
    mainSocket_ = ::socket(AF_INET, SOCK_STREAM, 0);
    if (mainSocket_ < 0) { 
        std::cerr << "Error 2: Socket creation failed" << std::endl; 
        return 2;
    } 

    struct sockaddr_in serverAddress;
    ::memset(&serverAddress, '0', sizeof(serverAddress));
    serverAddress.sin_family = AF_INET;
    serverAddress.sin_port = ::htons(PORT);

    /// Convert IPv4 and IPv6 addresses from text to binary form
    const int inetPtonResult = ::inet_pton(AF_INET, "127.0.0.1", &serverAddress.sin_addr);
    if (inetPtonResult < 0) {
        std::cerr << "Error 3: Invalid address. Address is not supported" << std::endl;
        return 3;
    }

    const int connectResult = ::connect(mainSocket_, (struct sockaddr *)&serverAddress, sizeof(serverAddress));
    if (connectResult < 0) {
        std::cerr << "Error 4: Cannot connect to server (" << errno << ")" << std::endl;
        return 4;
    }

    thread_ = std::thread(&ChatConnection::receive, this);

    while (true) {
        char message[MESSAGE_SIZE];
        std::cin.getline(message, MESSAGE_SIZE);

        const int sendSize = ::strlen(message) + 1;
        ::send(mainSocket_, &sendSize, sizeof(sendSize), 0);
        ::send(mainSocket_, message, sendSize, 0);
    }
    return 0;
}

int
ChatConnection::send(const char* message, const int messageSize)
{
    ::send(mainSocket_, &messageSize, sizeof(messageSize), 0);
    ::send(mainSocket_, message, messageSize, 0);
    return 0;
}

