We all use a watch. No matter if it is a wall-watch or a hand-watch it belongs to the ABSTRACT concept called "Watch".
It is called CLASS. The very watch that we use is an instance of this abstract class,
it is usually called an OBJECT(instance) of the class. A watch should have colour, size, information about current time.
These are its ATTRIBUTES. The way of showing the current time is its BEHAVOUR, i.e. it can be digital or analog.
An alarm clock is a type of watch(SUBCLASS), which has INHERITED the exact same basic attributes and behavour from its
"Father" class. But it has some extra features such as ringing. The watch can ring every hour it will be its MESSAGE to the user
about a round time. The user doesn't know anything about internal processes, and is not able to touch
the ENCAPSULATED insides of the watch. It's called INFORMATION HIDING.
The information about current time is a DATA MEMBER which is stored inside the Watch. The ringing feature is a DATA FUNCTIONS.
The user may adjust things ONLY by pushing buttons which are the user INTERFACE.
