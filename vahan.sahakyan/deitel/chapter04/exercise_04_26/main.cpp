#include <iostream>
#include <unistd.h>

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Insert 5-digit number: ";
    }
    int palindrome;
    std::cin >> palindrome;
    if (palindrome > 99999 || palindrome < 10000) {
        std::cerr << "ERROR 1: The number is not 5-digit!\n";
        return 1;
    }

    int duplicate = palindrome;
    int mirrored = 0;

    while (duplicate != 0) {
        int digit = duplicate % 10;
        mirrored = (mirrored * 10) + digit;
        duplicate /= 10;
    }

    if (palindrome == mirrored) {
        std::cout << "YES! " << palindrome << " is a palindrome!!!" << std::endl;
    } else {
        std::cout << "NO.. " << palindrome << " is not a palindrome.." << std::endl;
    }

    return 0;
}

