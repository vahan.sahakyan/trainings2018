#include <iostream>
#include <unistd.h>

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Insert the side size (1-20): ";
    }
    int side;
    std::cin >> side;
    if (side < 1 || side > 20) {
        std::cerr << "ERROR 1: Wrong side size.\n";
        return 1;
    }

    int row = 1;
    while (row <= side) {
        int column = 1;
        while (column <= side) {
            if (1 == row) {
                std::cout << "*";
            } else if (side == row) {
                std::cout << "*";
            } else if (1 == column) {
                std::cout << "*";
            } else if (side == column) {
                std::cout << "*";
            } else {
                std::cout << " ";
            }
            ++column;
        }
        ++row;
        std::cout << std::endl;
    }
    return 0;
}

