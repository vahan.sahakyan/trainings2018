2.8 Fill in the blanks in each of the following:

a. COMMENTS are used to document a program and improve its readability.
b. The object used to print information on the screen is STANDARD OUTPUT STREAM OBJECT.
c. A C++ statement that makes a decision is A CONDITION.
d. Most calculations are normally performed by ASSIGNMENT statements.
e. The STANDARD INPUT STREAM object inputs values from the keyboard.
