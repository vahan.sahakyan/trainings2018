///This Program simulates an Armenian Common Dialog.
#include <iostream>
#include <string>

int
main()
{
    std::cout << "Program: Barlus apeee!! Inshkaaa?\nYou: "; ///show interest about life in general
    std::string response = "";
    std::getline(std::cin, response); ///wait for response
    while (response == "") {
        std::cout << "Program: Harc em tvel.. patasxani \nYou: "; ///show some anger, in case of getting no response
        std::getline(std::cin, response);
    }
    if (response == "ban che du asa") { ///a typical Armenian response with a counter-question
        std::cout << "Program: Normal eli ynger, yola.." << std::endl; ///a typical Armenian answer to the typical counter-question
    } else if (response != "") {
        std::cout << "Program: Parza axpers.." << std::endl; ///a common answer to any complicated(or not expected) response
    }
    return 0; ///interests satisfied
}


