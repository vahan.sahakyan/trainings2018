What is a header file? What is a source-code file? Discuss the purpose of each.

A header file has ".h" extension and usually contains a class's
Interface with function prototypes. A source-code file has ".cpp" extension, and here it includes function implementation. Another source-code file also contains the main function. The central idea in case of both files is software reusability. When having function prototypes in a separate header file, one won't be obliged to change function declarations in case of any changes in the
function body. In this case, the user of the class will be provided only the header file. Hence he/she will be unaware of functions implementations and will not code based on class implementation. 
