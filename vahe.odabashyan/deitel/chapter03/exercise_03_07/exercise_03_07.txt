Explain the purpose of a data member.

Each object of a class has its own copy of attributes which are specified by data members. When a class has a data member, the later can be modified at any time during program execution. In the opposite case, we will be obliged to declare a local variable for each member function that can be accessed only by the particular function. The existence of data members enables the data hiding concept which in its turn facilitates program debugging.  
