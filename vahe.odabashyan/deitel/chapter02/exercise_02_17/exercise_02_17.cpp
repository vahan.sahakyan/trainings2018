/// A program that prints the numbers 1 through 4 on the same line in different ways

#include <iostream> /// allows the program to input and output data

/// function main begins program execution

int
main()
{
    std::cout << "1 2 3 4" << std::endl; /// using one statement with one insertion operator

    std::cout << "1 " << "2 " << "3 " << "4" << std::endl; /// one statement with 4 insertion operators

    std::cout << "1 ";
    std::cout << "2 ";
    std::cout << "3 ";
    std::cout << "4" << std::endl; /// using four statements
    
    return 0; /// indicates the successful completion of the program
}

